package com.tcs.imet.delegator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.peregrine.servicecenter.pws.IncidentManagement;
import com.peregrine.servicecenter.pws.RetrieveIncidentRequest;
import com.peregrine.servicecenter.pws.RetrieveIncidentResponse;

@Component
public class HPSMDelegator {

	@Autowired
	private IncidentManagement hpsmService;

	public RetrieveIncidentResponse retrieveIncidents(
			RetrieveIncidentRequest retrieveIncidentRequest) {
		RetrieveIncidentResponse response = null;
		try {
			response = hpsmService.retrieveIncident(retrieveIncidentRequest);
		} catch (Exception exception) {
		}
		return response;
	}
}
