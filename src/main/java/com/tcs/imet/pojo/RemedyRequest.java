package com.tcs.imet.pojo;

/*
import hpd_incidentinterface_ws.ContactSensitivityType;
import hpd_incidentinterface_ws.PriorityType;
import hpd_incidentinterface_ws.ReportedSourceType;
import hpd_incidentinterface_ws.ServiceTypeType;
import hpd_incidentinterface_ws.StatusReasonType;
import hpd_incidentinterface_ws.StatusType;
import hpd_incidentinterface_ws.VIPType;
*/

public class RemedyRequest {
	
	java.lang.String qualification;
	
	java.lang.String startRecord;
	
	java.lang.String maxLimit;

	public java.lang.String getQualification() {
		return qualification;
	}

	public void setQualification(java.lang.String qualification) {
		this.qualification = qualification;
	}

	public java.lang.String getStartRecord() {
		return startRecord;
	}

	public void setStartRecord(java.lang.String startRecord) {
		this.startRecord = startRecord;
	}

	public java.lang.String getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(java.lang.String maxLimit) {
		this.maxLimit = maxLimit;
	}
	
}

	
	/*
	
	java.lang.String assignedGroup;

	java.lang.String assignedGroupShiftName;

	java.lang.String assignedSupportCompany;

	java.lang.String assignedSupportOrganization;

	java.lang.String assignee;

	java.lang.String categorizationTier1;

	java.lang.String categorizationTier2;

	java.lang.String categorizationTier3;

	java.lang.String city;

	java.lang.String closureManufacturer;

	java.lang.String closureProductCategoryTier1;

	java.lang.String closureProductCategoryTier2;

	java.lang.String closureProductCategoryTier3;

	java.lang.String closureProductModelVersion;

	java.lang.String closureProductName;

	java.lang.String company;

	java.lang.String contactCompany;

	ContactSensitivityType contactSensitivity;

	java.lang.String country;

	java.lang.String department;

	java.lang.String summary;

	java.lang.String notes;

	java.lang.String firstName;

	java.lang.String impact;

	java.lang.String lastName;

	java.lang.String manufacturer;

	java.lang.String middleInitial;

	java.lang.String organization;

	java.lang.String phoneNumber;

	PriorityType priority;

	java.math.BigInteger priorityWeight;

	java.lang.String productCategorizationTier1;

	java.lang.String productCategorizationTier2;

	java.lang.String productCategorizationTier3;

	java.lang.String productModelVersion;

	java.lang.String productName;

	java.lang.String region;

	ReportedSourceType reportedSource;

	java.lang.String resolution;

	java.lang.String resolutionCategory;

	java.lang.String resolutionCategoryTier2;

	java.lang.String resolutionCategoryTier3;

	ServiceTypeType serviceType;

	java.lang.String site;

	java.lang.String siteGroup;

	StatusType status;

	StatusReasonType statusReason;

	java.lang.String urgency;

	VIPType vip;

	java.lang.String serviceCI;

	java.lang.String serviceCIReconID;

	java.lang.String hpdci;

	java.lang.String hpdciReconID;

	java.lang.String hpdciFormName;

	java.lang.String z1DCIFormName;

	public java.lang.String getAssignedGroup() {
		return assignedGroup;
	}

	public void setAssignedGroup(java.lang.String assignedGroup) {
		this.assignedGroup = assignedGroup;
	}

	public java.lang.String getAssignedGroupShiftName() {
		return assignedGroupShiftName;
	}

	public void setAssignedGroupShiftName(java.lang.String assignedGroupShiftName) {
		this.assignedGroupShiftName = assignedGroupShiftName;
	}

	public java.lang.String getAssignedSupportCompany() {
		return assignedSupportCompany;
	}

	public void setAssignedSupportCompany(java.lang.String assignedSupportCompany) {
		this.assignedSupportCompany = assignedSupportCompany;
	}

	public java.lang.String getAssignedSupportOrganization() {
		return assignedSupportOrganization;
	}

	public void setAssignedSupportOrganization(java.lang.String assignedSupportOrganization) {
		this.assignedSupportOrganization = assignedSupportOrganization;
	}

	public java.lang.String getAssignee() {
		return assignee;
	}

	public void setAssignee(java.lang.String assignee) {
		this.assignee = assignee;
	}

	public java.lang.String getCategorizationTier1() {
		return categorizationTier1;
	}

	public void setCategorizationTier1(java.lang.String categorizationTier1) {
		this.categorizationTier1 = categorizationTier1;
	}

	public java.lang.String getCategorizationTier2() {
		return categorizationTier2;
	}

	public void setCategorizationTier2(java.lang.String categorizationTier2) {
		this.categorizationTier2 = categorizationTier2;
	}

	public java.lang.String getCategorizationTier3() {
		return categorizationTier3;
	}

	public void setCategorizationTier3(java.lang.String categorizationTier3) {
		this.categorizationTier3 = categorizationTier3;
	}

	public java.lang.String getCity() {
		return city;
	}

	public void setCity(java.lang.String city) {
		this.city = city;
	}

	public java.lang.String getClosureManufacturer() {
		return closureManufacturer;
	}

	public void setClosureManufacturer(java.lang.String closureManufacturer) {
		this.closureManufacturer = closureManufacturer;
	}

	public String getClosureProductCategoryTier1() {
		return closureProductCategoryTier1;
	}

	public void setClosureProductCategoryTier1(String closureProductCategoryTier1) {
		this.closureProductCategoryTier1 = closureProductCategoryTier1;
	}

	public java.lang.String getClosureProductCategoryTier2() {
		return closureProductCategoryTier2;
	}

	public void setClosureProductCategoryTier2(java.lang.String closureProductCategoryTier2) {
		this.closureProductCategoryTier2 = closureProductCategoryTier2;
	}

	public String getClosureProductCategoryTier3() {
		return closureProductCategoryTier3;
	}

	public void setClosureProductCategoryTier3(String closureProductCategoryTier3) {
		this.closureProductCategoryTier3 = closureProductCategoryTier3;
	}

	public java.lang.String getClosureProductModelVersion() {
		return closureProductModelVersion;
	}

	public void setClosureProductModelVersion(java.lang.String closureProductModelVersion) {
		this.closureProductModelVersion = closureProductModelVersion;
	}

	public java.lang.String getClosureProductName() {
		return closureProductName;
	}

	public void setClosureProductName(java.lang.String closureProductName) {
		this.closureProductName = closureProductName;
	}

	public java.lang.String getCompany() {
		return company;
	}

	public void setCompany(java.lang.String company) {
		this.company = company;
	}

	public java.lang.String getContactCompany() {
		return contactCompany;
	}

	public void setContactCompany(java.lang.String contactCompany) {
		this.contactCompany = contactCompany;
	}

	public ContactSensitivityType getContactSensitivity() {
		return contactSensitivity;
	}

	public void setContactSensitivity(ContactSensitivityType contactSensitivity) {
		this.contactSensitivity = contactSensitivity;
	}

	public java.lang.String getCountry() {
		return country;
	}

	public void setCountry(java.lang.String country) {
		this.country = country;
	}

	public java.lang.String getDepartment() {
		return department;
	}

	public void setDepartment(java.lang.String department) {
		this.department = department;
	}

	public java.lang.String getSummary() {
		return summary;
	}

	public void setSummary(java.lang.String summary) {
		this.summary = summary;
	}

	public java.lang.String getNotes() {
		return notes;
	}

	public void setNotes(java.lang.String notes) {
		this.notes = notes;
	}

	public java.lang.String getFirstName() {
		return firstName;
	}

	public void setFirstName(java.lang.String firstName) {
		this.firstName = firstName;
	}

	public java.lang.String getImpact() {
		return impact;
	}

	public void setImpact(java.lang.String impact) {
		this.impact = impact;
	}

	public java.lang.String getLastName() {
		return lastName;
	}

	public void setLastName(java.lang.String lastName) {
		this.lastName = lastName;
	}

	public java.lang.String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(java.lang.String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public java.lang.String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(java.lang.String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public java.lang.String getOrganization() {
		return organization;
	}

	public void setOrganization(java.lang.String organization) {
		this.organization = organization;
	}

	public java.lang.String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(java.lang.String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public PriorityType getPriority() {
		return priority;
	}

	public void setPriority(PriorityType priority) {
		this.priority = priority;
	}

	public java.math.BigInteger getPriorityWeight() {
		return priorityWeight;
	}

	public void setPriorityWeight(java.math.BigInteger priorityWeight) {
		this.priorityWeight = priorityWeight;
	}

	public java.lang.String getProductCategorizationTier1() {
		return productCategorizationTier1;
	}

	public void setProductCategorizationTier1(java.lang.String productCategorizationTier1) {
		this.productCategorizationTier1 = productCategorizationTier1;
	}

	public java.lang.String getProductCategorizationTier2() {
		return productCategorizationTier2;
	}

	public void setProductCategorizationTier2(java.lang.String productCategorizationTier2) {
		this.productCategorizationTier2 = productCategorizationTier2;
	}

	public java.lang.String getProductCategorizationTier3() {
		return productCategorizationTier3;
	}

	public void setProductCategorizationTier3(java.lang.String productCategorizationTier3) {
		this.productCategorizationTier3 = productCategorizationTier3;
	}

	public java.lang.String getProductModelVersion() {
		return productModelVersion;
	}

	public void setProductModelVersion(java.lang.String productModelVersion) {
		this.productModelVersion = productModelVersion;
	}

	public java.lang.String getProductName() {
		return productName;
	}

	public void setProductName(java.lang.String productName) {
		this.productName = productName;
	}

	public java.lang.String getRegion() {
		return region;
	}

	public void setRegion(java.lang.String region) {
		this.region = region;
	}

	public ReportedSourceType getReportedSource() {
		return reportedSource;
	}

	public void setReportedSource(ReportedSourceType reportedSource) {
		this.reportedSource = reportedSource;
	}

	public java.lang.String getResolution() {
		return resolution;
	}

	public void setResolution(java.lang.String resolution) {
		this.resolution = resolution;
	}

	public java.lang.String getResolutionCategory() {
		return resolutionCategory;
	}

	public void setResolutionCategory(java.lang.String resolutionCategory) {
		this.resolutionCategory = resolutionCategory;
	}

	public java.lang.String getResolutionCategoryTier2() {
		return resolutionCategoryTier2;
	}

	public void setResolutionCategoryTier2(java.lang.String resolutionCategoryTier2) {
		this.resolutionCategoryTier2 = resolutionCategoryTier2;
	}

	public java.lang.String getResolutionCategoryTier3() {
		return resolutionCategoryTier3;
	}

	public void setResolutionCategoryTier3(java.lang.String resolutionCategoryTier3) {
		this.resolutionCategoryTier3 = resolutionCategoryTier3;
	}

	public ServiceTypeType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceTypeType serviceType) {
		this.serviceType = serviceType;
	}

	public java.lang.String getSite() {
		return site;
	}

	public void setSite(java.lang.String site) {
		this.site = site;
	}

	public java.lang.String getSiteGroup() {
		return siteGroup;
	}

	public void setSiteGroup(java.lang.String siteGroup) {
		this.siteGroup = siteGroup;
	}

	public StatusType getStatus() {
		return status;
	}

	public void setStatus(StatusType status) {
		this.status = status;
	}

	public StatusReasonType getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(StatusReasonType statusReason) {
		this.statusReason = statusReason;
	}

	public java.lang.String getUrgency() {
		return urgency;
	}

	public void setUrgency(java.lang.String urgency) {
		this.urgency = urgency;
	}

	public VIPType getVip() {
		return vip;
	}

	public void setVip(VIPType vip) {
		this.vip = vip;
	}

	public java.lang.String getServiceCI() {
		return serviceCI;
	}

	public void setServiceCI(java.lang.String serviceCI) {
		this.serviceCI = serviceCI;
	}

	public java.lang.String getServiceCIReconID() {
		return serviceCIReconID;
	}

	public void setServiceCIReconID(java.lang.String serviceCIReconID) {
		this.serviceCIReconID = serviceCIReconID;
	}

	public java.lang.String getHpdci() {
		return hpdci;
	}

	public void setHpdci(java.lang.String hpdci) {
		this.hpdci = hpdci;
	}

	public java.lang.String getHpdciReconID() {
		return hpdciReconID;
	}

	public void setHpdciReconID(java.lang.String hpdciReconID) {
		this.hpdciReconID = hpdciReconID;
	}

	public java.lang.String getHpdciFormName() {
		return hpdciFormName;
	}

	public void setHpdciFormName(java.lang.String hpdciFormName) {
		this.hpdciFormName = hpdciFormName;
	}

	public java.lang.String getZ1DCIFormName() {
		return z1DCIFormName;
	}

	public void setZ1DCIFormName(java.lang.String z1dciFormName) {
		z1DCIFormName = z1dciFormName;
	}



}
*/